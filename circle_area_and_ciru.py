#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 19:49:55 2021

@author: prime
"""
import math

print("Welcome to the circulator system . . .")

radius = float(input("Insert the radius(cm): "))

area = math.pi * radius**2
circum = 2 * math.pi * radius

print("Area of {}cm radius circle = {}cm^2.".format(radius, round(area, 2)))
print("Circum of {}cm radius circle = {}cm.".format(radius, round(circum, 2)))