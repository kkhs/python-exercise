#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  7 13:00:28 2021

@author: prime
"""

print("Welcome to the grade clac program")

grade1 = float(input("Enter Grade A: "))
grade2 = float(input("Enter Grade B: "))
absent = int(input("Enter Absences: "))
total_class = int(input("Enter total classes: "))

avg_grade = (grade1 + grade2) / 2
avg_attendence = (total_class - absent) / total_class

if(avg_grade >= 6):
    if(avg_attendence >= 0.8):
        print("You are passed ")
    else:
        print("You are failed due to attendence")
elif(avg_attendence <= 0.8):
    print("You are failed due to lower grade than 0.6")
else:
    print("You are failed due to lower attendence")