#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  7 13:24:38 2021

@author: prime
"""

print("Welcome to the BMI Calc Program . . . ")

height_in = float(input("Enter your height in inches: "))
weight_lb = float(input("Enter your weight in pound: "))

height = height_in / 39.37
weight = weight_lb / 2.205

bmi = round(weight / (height * height), 2)

if(bmi <= 18.5):
    print("Your BMI = {} \nYou are underweight.".format(bmi))
elif(bmi > 18.5 or bmi <= 24.9):
    print("Your BMI = {} \nYou are Normal Weight.".format(bmi))
elif(bmi > 25.9 or bmi <= 29.9):
    print("Your BMI = {} \nYou are Over Wight.".format(bmi))
else:
    print("Your BMI = {} \nObesity!".format(bmi))