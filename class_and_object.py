# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

class ShowYourSelf: #just blueprint
    
    def __init__(self, name="", age=0):
        print("Constructor is running . . .")
        self.real_name = name
        self.real_age = age
        
        
    def tellMyName(self): #methods
        print("I'm {} and {} years old".format(self.real_name, self.real_age))
        
        
aungaung = ShowYourSelf("Aung Aung", 30)  #object init
aungaung.tall = "4 feet"


mgmg = ShowYourSelf("Mg Mg", 20) #object init
mgmg.hobby = "Swmming"


print(aungaung.tall)
print(mgmg.hobby)