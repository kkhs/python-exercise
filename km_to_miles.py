#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 13:56:25 2021

@author: prime
"""

print("Welcome to the Converter Program")

standard = 1.60934
convertTo = int(input("1.km to miles \n2.miles to km \n"))
your_unit = float(input("Insert Unit: "))

print("\n1mile = 1.60934km")

if convertTo == 1:
    final_unit = your_unit / standard
    print("{} km is equal to {} miles".format(your_unit, final_unit))
elif convertTo == 2:
    final_unit = your_unit * standard
    print("{} mile is equal to {} km".format(your_unit, final_unit))
else:
    print("Your input is invalid")