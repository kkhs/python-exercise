#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 19:18:43 2021

@author: prime
"""

firstName = input("Enter your first name: ")
middleName = input("Enter your middle name: ")
lastName = input("Enter your last name: ")

print("Your initials are {} {} {}.".format(
        firstName[0], middleName[0], lastName[0]
        )
)