#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 19:29:07 2021

@author: prime
"""

callNumber = input("Enter Country Code '034-33333-33333': ")
splitCode = callNumber.split("-")

print("Country Code: {}".format(splitCode[0]))
print("Product Code: {}".format(splitCode[1]))
print("Batch Code: {}".format(splitCode[2]))