#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct  6 21:36:23 2021

@author: prime
"""

ahsaung = {
            "name" : "Saung",
            "age" : 21,
            "gender" : "female",
            "address" : "ကိုကြီးရင်ခွင်ထဲ",
            "phone" : "09123123123"
        }

print("Welcome user '{}' . . .".format(ahsaung['name']))
quest = input("Check Your Information: ")
result = ahsaung.get(quest, "သတ်မှတ်ချက် ကျော်လွန်သောစစ်ဆေးမှု")
print("Your {} is {}.".format(quest, result))